package com.example.restservice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class NameInfoTests {

    @Test
    void zeroLength() {
        assertEquals(0, NameInfo.getNameLength(""));
    }

    @Test
    void nonZeroLength() {
        assertEquals(5, NameInfo.getNameLength("World"));
    }

    @Test
    void nullLength() {
        assertThrows(NullPointerException.class, () -> {
            NameInfo.getNameLength(null);
        });
    }

    @Test
    void instantiating() {
        assertThrows(UnsupportedOperationException.class, NameInfo::new);
    }
}