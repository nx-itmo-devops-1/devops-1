package com.example.restservice;

public class NameInfo {

	NameInfo() {
		throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
	}

	public static long getNameLength(String name) {
		return name.length();
	}
}
