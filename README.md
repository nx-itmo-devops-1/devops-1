**A Sample Spring Boot Application**

This is a sample application taken from https://spring.io/guides/gs/rest-service/.

It will accept HTTP GET requests at http://localhost:8080/greeting and respond with a JSON representation of a greeting, as the following listing shows:

```json
{"id":1,"content":"Hello, World!"}
```

You can customize the greeting with an optional name parameter in the query string, as the following listing shows:

```
http://localhost:8080/greeting?name=User
```

The name parameter value overrides the default value of World and is reflected in the response, as the following listing shows:

```json
{"id":2,"content":"Hello, User!"}
```

The `id` parameter represents the ordinal number of the generated greeting.

**Usage**

To run this application:

- make sure you have JDK and Maven installed
- checkout the project code
- `cd` into your project directory
- `mvn clean package`
- `java -jar target/rest-service-0.0.1-SNAPSHOT.jar`


Test TeamCity builds